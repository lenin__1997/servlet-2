package ser;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Servlet implementation class register
 */
public class register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public register() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/db3","root","root");
			Statement state = connect.createStatement();
			ResultSet result = state.executeQuery("select * from data");
			PrintWriter out = response.getWriter();
			while (result.next()) {
				out.println(result.getString("name"));
				out.println(result.getString("password"));
			}

		}
		catch(Exception e ) {
			e.printStackTrace();
			}
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/db3","root","root");
			PreparedStatement preS = connect.prepareStatement("insert into data values ( ?,?)");
			preS.setString(1, name);
			preS.setString(2, password);
			preS.executeUpdate();
			response.sendRedirect("success.jsp");			
		}
		catch(Exception e ) {
			e.printStackTrace();
			}
	}

}




